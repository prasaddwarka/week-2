package springjdbc;

import org.springframework.jdbc.core.JdbcTemplate;

public class userDioimpl implements userDio{

    private JdbcTemplate jdbcTemplate;

    @Override
    public int insert(User user) {
        String query= "insert into user(id,name,email) values(?,?,?)";
        int result = this.jdbcTemplate.update(query,user.getId(),user.getName(),user.getEmail());
        return result;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

}

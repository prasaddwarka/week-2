/*
created By:Dwarka Prasad Bairwa
 */

package springjdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class App{
    public static void main(String[] args) {
        System.out.println("Hello World!");

        //spring jdbc
        ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
        //userDio ud = context.getBean("UserDio",userDio.class);
        JdbcTemplate templet = context.getBean("jdbcTemplet",JdbcTemplate.class);

        //Execute command
        //templet.execute("create table user(id int primary key, name varchar(50), email varchar(50");

        //insert Query
        String query= "insert into user(id,name,email) values(?,?,?)";
        User user = new User(3,"Rahul","Rahul@gmail.com");
        //int result = ud.insert(user);
        //int result = templet.update(query,user.getId(),user.getName(),user.getEmail());
        //System.out.println("Number of Record Inserted : " + result);

        //select Query
        //String select = "select * from user";
        //List<Map<String,Object>> result = templet.queryForList(select);
        //System.out.println("DataBase Are + "+result);

        //batch update in jdbcTemplet
        User user1 = new User(4,"Samay","Samay@gmail.com");
        User user2 = new User(5,"Vishnu","Vishnu@gmail.com");
        User user3 = new User(6,"Tanvi","Tanvi@gmail.com");

        List<User> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);

        String BatchQuery = "insert into user(id,name,email) values(?,?,?)";
        List<Object[]> batchArg = new ArrayList<Object[]>();

        for(User u:list){
            Object[] objectArr = {
                    u.getId(),u.getName(),u.getEmail()};
            batchArg.add(objectArr);
        }

        int[] result = templet.batchUpdate(BatchQuery,batchArg);
    }
}

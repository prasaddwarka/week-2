package Example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App{
    public static void main(String[] args) {

        //Application Context Example
        ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
        Vehicle obj = (Vehicle)context.getBean("vehicle");
        obj.drive();
    }
}

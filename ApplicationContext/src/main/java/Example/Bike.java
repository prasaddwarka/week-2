package Example;

public class Bike implements Vehicle{
    @Override
    public void drive() {
        System.out.println("Welcome to Bike Ride!");
    }
}

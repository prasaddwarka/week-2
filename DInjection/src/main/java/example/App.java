/*
created By: Dwarka Prasad Bairwa
 */
package example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args){

        //Beans From XML File
        //ApplicationContext context = new ClassPathXmlApplicationContext("Appconfig.xml");
        //Samsung s7 = context.getBean("samsung",Samsung.class);

        //Beans From Annotation
        ApplicationContext ctx= new AnnotationConfigApplicationContext(Appconfig.class);
        Samsung s8 = ctx.getBean(Samsung.class);

        //s7.config();
        s8.config();

    }
}

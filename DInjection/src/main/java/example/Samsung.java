package example;

import org.springframework.beans.factory.annotation.Autowired;

public class Samsung {

    @Autowired
    MobileProcessor cpu;

    public Samsung(){

    }

    //constructor DI Injection
    public Samsung(MobileProcessor cpu) {
        this.cpu = cpu;
    }

    //setter DI injection
    public MobileProcessor getCpu() {
        return cpu;
    }

    public void setCpu(MobileProcessor cpu) {
        this.cpu = cpu;
    }

    public void config(){
        System.out.println("Hello World!");
        cpu.process();
    }
}

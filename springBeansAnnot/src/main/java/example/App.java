/*
created By: Dwarka Prasad Bairwa
 */
package example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args){

        //Beans From XML File
        //ApplicationContext context = new ClassPathXmlApplicationContext("Appconfig.xml");
        //Samsung s7 = context.getBean("samsung",Samsung.class);

        //Beans From Annotation
        ApplicationContext ctx= new AnnotationConfigApplicationContext(Appconfig.class);

        //Beans
        //AnnotationConfigApplicationContext ctx2 = new AnnotationConfigApplicationContext();
        //ctx2.register(Appconfig.class);
        //ctx2.refresh();
        //Samsung s8 = context.getBean(Samsung.class);

        /* -- Calling through Alias --*/

        Samsung s8 = (Samsung)ctx.getBean("MyBeans");

        //s7.config();
        s8.config();

    }
}
